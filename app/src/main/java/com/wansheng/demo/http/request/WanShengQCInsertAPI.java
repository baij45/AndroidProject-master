package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;
import com.wansheng.demo.http.response.QCBean;

public class WanShengQCInsertAPI implements IRequestApi {
    private QCBean qcBean;

    public QCBean getQcBean() {
        return qcBean;
    }

    public WanShengQCInsertAPI setQcBean(QCBean qcBean) {
        this.qcBean = qcBean;
        return this;
    }

    @Override
    public String getApi() {
        return "insertQC";
    }
}
