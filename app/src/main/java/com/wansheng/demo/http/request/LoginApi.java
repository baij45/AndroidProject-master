package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 用户登录
 */
public final class LoginApi implements IRequestApi {

    @Override
    public String getApi() {
        return "longin";
    }

    /** 手机号 */
    private String id;

    public LoginApi setId(String id) {
        this.id = id;
        return this;
    }

}