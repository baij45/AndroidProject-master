package com.wansheng.demo.http.response;

import java.math.BigDecimal;

public class BGResponseBean {
    private String name;
    private BigDecimal 最大可报数量;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal get最大可报数量() {
        return 最大可报数量;
    }

    public void set最大可报数量(BigDecimal 最大可报数量) {
        this.最大可报数量 = 最大可报数量;
    }
}
