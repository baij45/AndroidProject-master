package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;

public class WanShengQueryQCAPI implements IRequestApi {
    private String 生产订单号;
    private String 生产订单行号;

    public String get生产订单号() {
        return 生产订单号;
    }

    public WanShengQueryQCAPI set生产订单号(String 生产订单号) {
        this.生产订单号 = 生产订单号;
        return this;
    }

    public String get生产订单行号() {
        return 生产订单行号;
    }

    public WanShengQueryQCAPI set生产订单行号(String 生产订单行号) {
        this.生产订单行号 = 生产订单行号;
        return this;
    }

    @Override
    public String getApi() {
        return "getSubmitted";
    }

}
