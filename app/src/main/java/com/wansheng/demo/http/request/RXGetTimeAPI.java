package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;

public class RXGetTimeAPI implements IRequestApi {

    @Override
    public String getApi() {
        return "getTime";
    }
}
