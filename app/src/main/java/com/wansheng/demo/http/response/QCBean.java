package com.wansheng.demo.http.response;

import java.math.BigDecimal;
import java.sql.Date;

public class QCBean {
    private String 生产订单号;
    private String 生产订单行号;
    private String 物料编号;
    private BigDecimal 订单数量;
    private BigDecimal 报工数量;
    private String 加工顺序;
    private String 工序编号;
    private String 工序名称;
    private String 机台;
    private String baogongid;
    private BigDecimal 合格数量;
    private BigDecimal 不良数量;
    private String 质检人员;
    private String 质检时间;
    private String 不合格原因;
    private String 警告原因;

    public String get警告原因() {
        return 警告原因;
    }

    public void set警告原因(String 警告原因) {
        this.警告原因 = 警告原因;
    }

    public String get不合格原因() {
        return 不合格原因;
    }

    public void set不合格原因(String 不合格原因) {
        this.不合格原因 = 不合格原因;
    }

    public String get生产订单号() {
        return 生产订单号;
    }

    public void set生产订单号(String 生产订单号) {
        this.生产订单号 = 生产订单号;
    }

    public String get生产订单行号() {
        return 生产订单行号;
    }

    public void set生产订单行号(String 生产订单行号) {
        this.生产订单行号 = 生产订单行号;
    }

    public String get物料编号() {
        return 物料编号;
    }

    public void set物料编号(String 物料编号) {
        this.物料编号 = 物料编号;
    }

    public BigDecimal get订单数量() {
        return 订单数量;
    }

    public void set订单数量(BigDecimal 订单数量) {
        this.订单数量 = 订单数量;
    }

    public String get加工顺序() {
        return 加工顺序;
    }

    public void set加工顺序(String 加工顺序) {
        this.加工顺序 = 加工顺序;
    }

    public String get工序编号() {
        return 工序编号;
    }

    public void set工序编号(String 工序编号) {
        this.工序编号 = 工序编号;
    }

    public String get工序名称() {
        return 工序名称;
    }

    public void set工序名称(String 工序名称) {
        this.工序名称 = 工序名称;
    }

    public String getBaogongid() {
        return baogongid;
    }

    public void setBaogongid(String baogongid) {
        this.baogongid = baogongid;
    }

    public BigDecimal get合格数量() {
        return 合格数量;
    }

    public void set合格数量(BigDecimal 合格数量) {
        this.合格数量 = 合格数量;
    }

    public BigDecimal get不良数量() {
        return 不良数量;
    }

    public void set不良数量(BigDecimal 不良数量) {
        this.不良数量 = 不良数量;
    }

    public String get质检人员() {
        return 质检人员;
    }

    public void set质检人员(String 质检人员) {
        this.质检人员 = 质检人员;
    }

    public String get质检时间() {
        return 质检时间;
    }

    public void set质检时间(String 质检时间) {
        this.质检时间 = 质检时间;
    }

    public String get机台() {
        return 机台;
    }

    public void set机台(String 机台) {
        this.机台 = 机台;
    }

    public BigDecimal get报工数量() {
        return 报工数量;
    }

    public void set报工数量(BigDecimal 报工数量) {
        this.报工数量 = 报工数量;
    }

    @Override
    public String toString() {
        return "QCBean{" +
                "生产订单号='" + 生产订单号 + '\'' +
                ", 生产订单行号='" + 生产订单行号 + '\'' +
                ", 物料编号='" + 物料编号 + '\'' +
                ", 订单数量='" + 订单数量 + '\'' +
                ", 报工数量='" + 报工数量 + '\'' +
                ", 加工顺序='" + 加工顺序 + '\'' +
                ", 工序编号='" + 工序编号 + '\'' +
                ", 工序名称='" + 工序名称 + '\'' +
                ", 机台='" + 机台 + '\'' +
                ", baogongid='" + baogongid + '\'' +
                ", 合格数量='" + 合格数量 + '\'' +
                ", 不良数量='" + 不良数量 + '\'' +
                ", 质检人员='" + 质检人员 + '\'' +
                ", 质检时间='" + 质检时间 + '\'' +
                ", 不合格原因='" + 不合格原因 + '\'' +
                ", 警告原因='" + 警告原因 + '\'' +
                '}';
    }
}
