package com.wansheng.demo.http.response;

import java.math.BigDecimal;
import java.sql.Date;

public class InsertBaoGong {
    private String 生产订单号;
    private String 生产订单行号;
    private String 物料编号;
    private BigDecimal 订单数量;
    private String 加工顺序;
    private String 工序名称;
    private BigDecimal 报工数量;
    private String 报工人员;
    private Date 报工时间;
    private String 机台;

    public String get生产订单号() {
        return 生产订单号;
    }

    public void set生产订单号(String 生产订单号) {
        this.生产订单号 = 生产订单号;
    }

    public String get生产订单行号() {
        return 生产订单行号;
    }

    public void set生产订单行号(String 生产订单行号) {
        this.生产订单行号 = 生产订单行号;
    }

    public String get物料编号() {
        return 物料编号;
    }

    public void set物料编号(String 物料编号) {
        this.物料编号 = 物料编号;
    }

    public BigDecimal get订单数量() {
        return 订单数量;
    }

    public void set订单数量(BigDecimal 订单数量) {
        this.订单数量 = 订单数量;
    }

    public String get加工顺序() {
        return 加工顺序;
    }

    public void set加工顺序(String 加工顺序) {
        this.加工顺序 = 加工顺序;
    }

    public String get工序名称() {
        return 工序名称;
    }

    public void set工序名称(String 工序名称) {
        this.工序名称 = 工序名称;
    }

    public BigDecimal get报工数量() {
        return 报工数量;
    }

    public void set报工数量(BigDecimal 报工数量) {
        this.报工数量 = 报工数量;
    }

    public String get报工人员() {
        return 报工人员;
    }

    public void set报工人员(String 报工人员) {
        this.报工人员 = 报工人员;
    }

    public Date get报工时间() {
        return 报工时间;
    }

    public void set报工时间(Date 报工时间) {
        this.报工时间 = 报工时间;
    }

    public String get机台() {
        return 机台;
    }

    public void set机台(String 机台) {
        this.机台 = 机台;
    }
}
