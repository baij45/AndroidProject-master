package com.wansheng.demo.http.response;

import androidx.annotation.NonNull;

public class User {
    private String 部门编号;
    private String 员工姓名;

    public String get部门编号() {
        return 部门编号;
    }

    public void set部门编号(String 部门编号) {
        this.部门编号 = 部门编号;
    }

    public String get员工姓名() {
        return 员工姓名;
    }

    public void set员工姓名(String 员工姓名) {
        this.员工姓名 = 员工姓名;
    }

    @Override
    public String toString() {
        return "User{" +
                "部门编号='" + 部门编号 + '\'' +
                ", 员工姓名='" + 员工姓名 + '\'' +
                '}';
    }
}
