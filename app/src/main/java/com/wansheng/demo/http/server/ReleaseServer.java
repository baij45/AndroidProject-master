package com.wansheng.demo.http.server;

import android.content.Context;
import android.content.SharedPreferences;

import com.hjq.http.config.IRequestServer;
import com.hjq.http.model.BodyType;
import com.wansheng.demo.helper.ActivityStackManager;

/**
 *    author : Android 轮子哥
 *    github : https://github.com/getActivity/AndroidProject
 *    time   : 2019/12/07
 *    desc   : 正式环境
 */
public class ReleaseServer implements IRequestServer {

    @Override
    public String getHost() {
        SharedPreferences sharedPreferences = ActivityStackManager.getInstance().getApplication().getSharedPreferences("ip", Context.MODE_PRIVATE);
        String ip = sharedPreferences.getString("ip","192.168.19.1");
        return "http://"+ip;
    }

    @Override
    public String getPath() {
        SharedPreferences sharedPreferences = ActivityStackManager.getInstance().getApplication().getSharedPreferences("port", Context.MODE_PRIVATE);
        String port = sharedPreferences.getString("port","8888");
        return ":"+port+"/";
    }

    @Override
    public BodyType getType() {
        return BodyType.JSON;
    }
}