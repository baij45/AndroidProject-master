package com.wansheng.demo.http.response;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class BaoGong {
    private String 生产加工单号;
    private String 生产加工单ID;

    private String 机台;

    private String 加工顺序;

    private String 工序编号;
    private String 工序名称;

    private String 物料编号;

    private BigDecimal 报工数量;
    private BigDecimal 可报数量;
    private BigDecimal 订单数量;

    //新加的字段
    private BigDecimal 工序单价;
    private Timestamp 开始时间;
    private Timestamp 结束时间;
    private BigDecimal 报工工时;
    private BigDecimal 工时单价;
    private BigDecimal 金额;
    private String 是否工时结算;
    private String 是否已经计算;
    private String 是否最大顺序;

    public String get生产加工单号() {
        return 生产加工单号;
    }

    public void set生产加工单号(String 生产加工单号) {
        this.生产加工单号 = 生产加工单号;
    }

    public String get生产加工单ID() {
        return 生产加工单ID;
    }

    public void set生产加工单ID(String 生产加工单ID) {
        this.生产加工单ID = 生产加工单ID;
    }

    public String get机台() {
        return 机台;
    }

    public void set机台(String 机台) {
        this.机台 = 机台;
    }

    public String get加工顺序() {
        return 加工顺序;
    }

    public void set加工顺序(String 加工顺序) {
        this.加工顺序 = 加工顺序;
    }

    public String get工序编号() {
        return 工序编号;
    }

    public void set工序编号(String 工序编号) {
        this.工序编号 = 工序编号;
    }

    public String get工序名称() {
        return 工序名称;
    }

    public void set工序名称(String 工序名称) {
        this.工序名称 = 工序名称;
    }

    public String get物料编号() {
        return 物料编号;
    }

    public void set物料编号(String 物料编号) {
        this.物料编号 = 物料编号;
    }

    public BigDecimal get报工数量() {
        return 报工数量;
    }

    public void set报工数量(BigDecimal 报工数量) {
        this.报工数量 = 报工数量;
    }

    public BigDecimal get可报数量() {
        return 可报数量;
    }

    public void set可报数量(BigDecimal 可报数量) {
        this.可报数量 = 可报数量;
    }

    public BigDecimal get订单数量() {
        return 订单数量;
    }

    public void set订单数量(BigDecimal 订单数量) {
        this.订单数量 = 订单数量;
    }

    public BigDecimal get工序单价() {
        return 工序单价;
    }

    public void set工序单价(BigDecimal 工序单价) {
        this.工序单价 = 工序单价;
    }

    public Timestamp get开始时间() {
        return 开始时间;
    }

    public void set开始时间(Timestamp 开始时间) {
        this.开始时间 = 开始时间;
    }

    public Timestamp get结束时间() {
        return 结束时间;
    }

    public void set结束时间(Timestamp 结束时间) {
        this.结束时间 = 结束时间;
    }

    public BigDecimal get报工工时() {
        return 报工工时;
    }

    public void set报工工时(BigDecimal 报工工时) {
        this.报工工时 = 报工工时;
    }

    public BigDecimal get工时单价() {
        return 工时单价;
    }

    public void set工时单价(BigDecimal 工时单价) {
        this.工时单价 = 工时单价;
    }

    public BigDecimal get金额() {
        return 金额;
    }

    public void set金额(BigDecimal 金额) {
        this.金额 = 金额;
    }

    public String get是否工时结算() {
        return 是否工时结算;
    }

    public void set是否工时结算(String 是否工时结算) {
        this.是否工时结算 = 是否工时结算;
    }

    public String get是否已经计算() {
        return 是否已经计算;
    }

    public void set是否已经计算(String 是否已经计算) {
        this.是否已经计算 = 是否已经计算;
    }

    public String get是否最大顺序() {
        return 是否最大顺序;
    }

    public void set是否最大顺序(String 是否最大顺序) {
        this.是否最大顺序 = 是否最大顺序;
    }

    @Override
    public String toString() {
        return "BaoGong{" +
                "生产加工单号='" + 生产加工单号 + '\'' +
                ", 生产加工单ID='" + 生产加工单ID + '\'' +
                ", 机台='" + 机台 + '\'' +
                ", 加工顺序='" + 加工顺序 + '\'' +
                ", 工序编号='" + 工序编号 + '\'' +
                ", 工序名称='" + 工序名称 + '\'' +
                ", 物料编号='" + 物料编号 + '\'' +
                ", 报工数量=" + 报工数量 +
                ", 可报数量=" + 可报数量 +
                ", 订单数量=" + 订单数量 +
                ", 工序单价=" + 工序单价 +
                ", 开始时间='" + 开始时间 + '\'' +
                ", 结束时间='" + 结束时间 + '\'' +
                ", 报工工时='" + 报工工时 + '\'' +
                ", 工时单价='" + 工时单价 + '\'' +
                ", 金额='" + 金额 + '\'' +
                ", 是否工时结算='" + 是否工时结算 + '\'' +
                ", 是否已经计算='" + 是否已经计算 + '\'' +
                ", 是否最大顺序='" + 是否最大顺序 + '\'' +
                '}';
    }
}
