package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;
import com.wansheng.demo.http.response.BaoGong;

public class WanShengBaoGongAPI implements IRequestApi {
    private String 生产加工单号;
    private String 生产加工单id;
    private String 机台;

    public String get生产加工单号() {
        return 生产加工单号;
    }

    public WanShengBaoGongAPI set生产加工单号(String 生产加工单号) {
        this.生产加工单号 = 生产加工单号;
        return this;
}

    public String get生产加工单id() {
        return 生产加工单id;
    }

    public WanShengBaoGongAPI set生产加工单id(String 生产加工单id) {
        this.生产加工单id = 生产加工单id;
        return this;
    }

    public String get机台() {
        return 机台;
    }

    public WanShengBaoGongAPI set机台(String 机台) {
        this.机台 = 机台;
        return this;
    }

    @Override
    public String getApi() {
        return "getGX";
    }


//    public WanShengBaoGongAPI setBaoGong(BaoGong baoGong){
//        this.baoGong = baoGong;
//        return this;
//    }
}
