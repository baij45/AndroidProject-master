package com.wansheng.demo.http.request;

import com.hjq.http.config.IRequestApi;
import com.wansheng.demo.http.response.BaoGong;

import java.math.BigDecimal;
import java.sql.Date;

public class WanShengBaoGongInsertAPI implements IRequestApi {
    private BigDecimal 报工数量;
    private String 报工人员;
    private String 报工时间;
    private String 机台;
    private BaoGong baoGong;

    public BigDecimal get报工数量() {
        return 报工数量;
    }

    public WanShengBaoGongInsertAPI set报工数量(BigDecimal 报工数量) {
        this.报工数量 = 报工数量;
        return this;
    }

    public String get报工人员() {
        return 报工人员;
    }

    public WanShengBaoGongInsertAPI set报工人员(String 报工人员) {
        this.报工人员 = 报工人员;
        return this;
    }

    public String get报工时间() {
        return 报工时间;
    }

    public WanShengBaoGongInsertAPI set报工时间(String 报工时间) {
        this.报工时间 = 报工时间;
        return this;
    }

    public String get机台() {
        return 机台;
    }

    public WanShengBaoGongInsertAPI set机台(String 机台) {
        this.机台 = 机台;
        return this;
    }

    public BaoGong getBaoGong() {
        return baoGong;
    }

    public WanShengBaoGongInsertAPI setBaoGong(BaoGong baoGong) {
        this.baoGong = baoGong;
        return this;
    }

    @Override
    public String getApi() {
        return "insertGX";
    }
}
