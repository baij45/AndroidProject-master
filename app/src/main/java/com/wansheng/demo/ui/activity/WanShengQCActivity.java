package com.wansheng.demo.ui.activity;

import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hjq.base.BaseDialog;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.wansheng.demo.R;
import com.wansheng.demo.aop.CheckNet;
import com.wansheng.demo.aop.SingleClick;
import com.wansheng.demo.common.MyActivity;
import com.wansheng.demo.helper.ActivityStackManager;
import com.wansheng.demo.helper.InputTextHelper;
import com.wansheng.demo.http.model.HttpData;
import com.wansheng.demo.http.request.WanShengQCInsertAPI;
import com.wansheng.demo.http.request.WanShengQueryQCAPI;
import com.wansheng.demo.http.response.QCBean;
import com.wansheng.demo.ui.dialog.InputDialog;
import com.wansheng.demo.ui.dialog.MessageDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WanShengQCActivity extends MyActivity {
    private TextView userNameView;
    private EditText danhao;
    private EditText jitai;
    private EditText wanchenggx;
    private EditText shuliang;
    private Button button_checkok;
    private Button button_checkbad;
    private Button button_warning;
    private Button button_clear;
    private Button button_currencheckok;
    private Button button_currencheckbad;
    private Button button_currenwarning;
    private EditText currentgx;
    String userName = "";
    String userCode = "";
    private QCBean qcBean = new QCBean();
    private QCBean current = new QCBean();

    @Override
    protected int getLayoutId() {
        return R.layout.activity_wan_sheng_q_c;
    }

    @Override
    protected void initView() {
        userNameView = findViewById(R.id.userName);
        danhao = findViewById(R.id.danhao);
        jitai = findViewById(R.id.jitai);
        wanchenggx = findViewById(R.id.wanchenggx);
        shuliang = findViewById(R.id.shuliang);
        button_checkok = findViewById(R.id.button_checkok);
        button_checkbad = findViewById(R.id.button_checkbad);
        button_warning = findViewById(R.id.button_warning);
        button_clear = findViewById(R.id.button_clear);
        button_currencheckok = findViewById(R.id.button_currencheckok);
        button_currencheckbad = findViewById(R.id.button_currencheckbad);
        button_currenwarning = findViewById(R.id.button_currenwarning);
        currentgx = findViewById(R.id.currentgx);
        danhao.setFocusable(true);
        danhao.requestFocus();
        setOnClickListener(button_checkok, button_checkbad, button_warning, button_clear, button_currencheckok, button_currencheckbad, button_currenwarning);
        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .setMain(button_checkok)
                .build();

        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .setMain(button_checkbad)
                .build();

        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .setMain(button_warning)
                .build();

        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .addView(currentgx)
                .setMain(button_currencheckok)
                .build();

        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .addView(currentgx)
                .setMain(button_currencheckbad)
                .build();

        InputTextHelper.with(this)
                .addView(danhao)
                .addView(jitai)
                .addView(wanchenggx)
                .addView(shuliang)
                .addView(currentgx)
                .setMain(button_currenwarning)
                .build();


//        danhao.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                KeyboardUtils.hideKeyboard(v);
//                danhao.requestFocus();
//                return true;
//            }
//        });

        //单号查询
        danhao.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (null != keyEvent && KeyEvent.KEYCODE_ENTER == keyEvent.getKeyCode() && keyEvent.getAction() == KeyEvent.ACTION_UP) {

                    if (danhao.getText().toString().indexOf("-", 0) > 0) {
                        int p1 = danhao.getText().toString().indexOf("-", 0);
                        String 生产加工单号 = danhao.getText().toString().substring(0, p1);
                        String 生产加工单id = danhao.getText().toString().substring(p1 + 1);
                        EasyHttp.post(WanShengQCActivity.this)
                                .api(new WanShengQueryQCAPI().set生产订单号(生产加工单号).set生产订单行号(生产加工单id))
                                .request(new HttpCallback<HttpData<List<QCBean>>>(WanShengQCActivity.this) {
                                    @Override
                                    public void onSucceed(HttpData<List<QCBean>> result) {
                                        qcBean = result.getData().get(0);
                                        jitai.setText(qcBean.get机台());
                                        wanchenggx.setText(qcBean.get工序名称());
                                        shuliang.setText(qcBean.get报工数量().toString());
                                        if (result.getData().size() > 1) {
                                            current = result.getData().get(1);
                                            currentgx.setText(result.getData().get(1).get工序名称());
                                        } else {
                                            currentgx.setText("");
                                        }
                                    }

                                    @Override
                                    public void onFail(Exception e) {
                                        jitai.setText("");
                                        wanchenggx.setText("");
                                        shuliang.setText("");
                                        currentgx.setText("");
                                        super.onFail(e);
                                    }
                                });
                    } else {
                        toast("此单号不符合格式");
                    }
                }
                return true;
            }
        });
    }

    @Override
    protected void initData() {
        //获得用户名
        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
        userCode = intent.getStringExtra("userCode");
        userNameView.setText("用户名:" + userName);
    }

    @CheckNet
    @SingleClick
    @Override
    public void onClick(View v) {
        if (v == button_checkok) {
            ok(qcBean);
        }
        if (v == button_checkbad) {
            bad(qcBean);
        }
        if (v == button_warning) {
            warning(qcBean);
        }
        if (v == button_clear) {
            clear();
        }

        if (v == button_currencheckok) {
            ok(current);
        }
        if (v == button_currencheckbad) {
            bad(current);
        }
        if (v == button_currenwarning) {
            warning(current);
        }
    }

    public void ok(QCBean qcBean) {
        qcBean.set质检人员(userCode);
        qcBean.set质检时间(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
        Log.i("qc", qcBean.toString());
        EasyHttp.post(this)
                .api(new WanShengQCInsertAPI().setQcBean(qcBean))
                .request(new HttpCallback<HttpData>(this) {
                    @Override
                    public void onSucceed(HttpData result) {
                        toast("质检成功");
                        danhao.setText("");
                        jitai.setText("");
                        wanchenggx.setText("");
                        shuliang.setText("");
                        currentgx.setText("");
                        danhao.requestFocus();
                    }
                });
    }

    public void bad(QCBean qcBean) {
        new InputDialog.Builder(this)
                // 标题可以不用填写
                .setTitle("检验不合格")
                // 内容可以不用填写
                // 提示可以不用填写
                .setHint("不合格原因")
                // 确定按钮文本
                .setConfirm(getString(R.string.common_confirm))
                // 设置 null 表示不显示取消按钮
                .setCancel(getString(R.string.common_cancel))
                // 设置点击按钮后不关闭对话框
                //.setAutoDismiss(false)
                .setListener(new InputDialog.OnListener() {

                    @Override
                    public void onConfirm(BaseDialog dialog, String content) {

                        // 消息对话框
                        new MessageDialog.Builder(WanShengQCActivity.this)
                                // 标题可以不用填写
                                .setTitle("不合格原因为：")
                                // 内容必须要填写
                                .setMessage(content)
                                // 确定按钮文本
                                .setConfirm(getString(R.string.common_confirm))
                                // 设置 null 表示不显示取消按钮
                                .setCancel(getString(R.string.common_cancel))
                                // 设置点击按钮后不关闭对话框
                                //.setAutoDismiss(false)
                                .setListener(new MessageDialog.OnListener() {

                                    @Override
                                    public void onConfirm(BaseDialog dialog) {
                                        qcBean.set不合格原因(content);
                                        qcBean.set质检人员(userCode);
                                        qcBean.set质检时间(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
                                        EasyHttp.post(WanShengQCActivity.this)
                                                .api(new WanShengQCInsertAPI().setQcBean(qcBean))
                                                .request(new HttpCallback<HttpData>(WanShengQCActivity.this) {
                                                    @Override
                                                    public void onSucceed(HttpData result) {
                                                        toast("质检不合格成功");
                                                        danhao.setText("");
                                                        jitai.setText("");
                                                        wanchenggx.setText("");
                                                        shuliang.setText("");
                                                        currentgx.setText("");
                                                        danhao.requestFocus();
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onCancel(BaseDialog dialog) {
                                        toast("取消了");
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onCancel(BaseDialog dialog) {
                        toast("取消了");
                    }
                })
                .show();
    }

    public void warning(QCBean qcBean) {
        // 输入对话框
        new InputDialog.Builder(this)
                // 标题可以不用填写
                .setTitle("检验警告")
                // 内容可以不用填写
                //.setContent("我是内容")
                // 提示可以不用填写
                .setHint("检验警告内容")
                // 确定按钮文本
                .setConfirm(getString(R.string.common_confirm))
                // 设置 null 表示不显示取消按钮
                .setCancel(getString(R.string.common_cancel))
                // 设置点击按钮后不关闭对话框
                //.setAutoDismiss(false)
                .setListener(new InputDialog.OnListener() {

                    @Override
                    public void onConfirm(BaseDialog dialog, String content) {
                        // 消息对话框
                        new MessageDialog.Builder(WanShengQCActivity.this)
                                // 标题可以不用填写
                                .setTitle("警告内容为：")
                                // 内容必须要填写
                                .setMessage(content)
                                // 确定按钮文本
                                .setConfirm(getString(R.string.common_confirm))
                                // 设置 null 表示不显示取消按钮
                                .setCancel(getString(R.string.common_cancel))
                                // 设置点击按钮后不关闭对话框
                                //.setAutoDismiss(false)
                                .setListener(new MessageDialog.OnListener() {

                                    @Override
                                    public void onConfirm(BaseDialog dialog) {
                                        qcBean.set警告原因(content);
                                        qcBean.set质检人员(userCode);
                                        qcBean.set质检时间(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())));
                                        EasyHttp.post(WanShengQCActivity.this)
                                                .api(new WanShengQCInsertAPI().setQcBean(qcBean))
                                                .request(new HttpCallback<HttpData>(WanShengQCActivity.this) {
                                                    @Override
                                                    public void onSucceed(HttpData result) {
                                                        toast("质检警告成功");
                                                        danhao.setText("");
                                                        jitai.setText("");
                                                        wanchenggx.setText("");
                                                        shuliang.setText("");
                                                        currentgx.setText("");
                                                        danhao.requestFocus();
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onCancel(BaseDialog dialog) {
                                        toast("取消了");
                                    }
                                })
                                .show();
                    }

                    @Override
                    public void onCancel(BaseDialog dialog) {
                        toast("取消了");
                    }
                })
                .show();
    }

    public void clear() {
        // 消息对话框
        new MessageDialog.Builder(this)
                // 标题可以不用填写
                .setTitle("警告")
                // 内容必须要填写
                .setMessage("确认清空所有内容?")
                // 确定按钮文本
                .setConfirm(getString(R.string.common_confirm))
                // 设置 null 表示不显示取消按钮
                .setCancel(getString(R.string.common_cancel))
                // 设置点击按钮后不关闭对话框
                //.setAutoDismiss(false)
                .setListener(new MessageDialog.OnListener() {

                    @Override
                    public void onConfirm(BaseDialog dialog) {
                        danhao.setText("");
                        jitai.setText("");
                        wanchenggx.setText("");
                        shuliang.setText("");
                        currentgx.setText("");
                        danhao.requestFocus();
                    }

                    @Override
                    public void onCancel(BaseDialog dialog) {
                        toast("取消了");
                    }
                })
                .show();
    }

    @SingleClick
    @Override
    public void onRightClick(View v) {
        if (true) {
            startActivity(LoginActivity.class);
            // 进行内存优化，销毁除登录页之外的所有界面
            ActivityStackManager.getInstance().finishAllActivities(LoginActivity.class);
            return;
        }
    }
}
