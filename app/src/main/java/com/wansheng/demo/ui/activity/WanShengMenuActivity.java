package com.wansheng.demo.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.wansheng.demo.R;
import com.wansheng.demo.aop.SingleClick;
import com.wansheng.demo.common.MyActivity;
import com.wansheng.demo.other.KeyboardWatcher;

public class WanShengMenuActivity extends MyActivity implements KeyboardWatcher.SoftKeyboardStateListener {

    private Button baogong;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_wan_sheng_menu;
    }

    @Override
    protected void initView() {
        baogong = findViewById(R.id.baogong);
        setOnClickListener(baogong);
    }

    @Override
    protected void initData() {

    }

    @SingleClick
    @Override
    public void onRightClick(View v) {
        startActivity(SettingActivity.class);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        if (v == baogong){
            startActivity(WanShengBaoGongActivity.class);
        }
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeight) {

    }

    @Override
    public void onSoftKeyboardClosed() {

    }
}
