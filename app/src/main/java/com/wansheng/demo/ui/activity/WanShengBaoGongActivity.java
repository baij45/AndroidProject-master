package com.wansheng.demo.ui.activity;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hjq.base.BaseDialog;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.wansheng.demo.R;
import com.wansheng.demo.aop.CheckNet;
import com.wansheng.demo.aop.SingleClick;
import com.wansheng.demo.common.MyActivity;
import com.wansheng.demo.helper.ActivityStackManager;
import com.wansheng.demo.helper.InputTextHelper;
import com.wansheng.demo.helper.KeyboardUtils;
import com.wansheng.demo.http.model.HttpData;
import com.wansheng.demo.http.request.RXGetTimeAPI;
import com.wansheng.demo.http.request.WanShengBaoGongAPI;
import com.wansheng.demo.http.request.WanShengBaoGongInsertAPI;
import com.wansheng.demo.http.response.BGResponseBean;
import com.wansheng.demo.http.response.BaoGong;
import com.wansheng.demo.other.KeyboardWatcher;
import com.wansheng.demo.ui.dialog.MessageDialog;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class WanShengBaoGongActivity extends MyActivity implements KeyboardWatcher.SoftKeyboardStateListener {

    private Button button_submit;
    private EditText caigoudanhao;
    private EditText jitaihao;
    private EditText gxName;
    private EditText kebaoshuliang;
    private EditText bencibaogong;
    private Button button_clear;
    private TextView view_userName;
    private TextView timer;
    String userName = "";
    String userCode = "";
    private BaoGong baoGong = new BaoGong();


    @Override
    protected int getLayoutId() {
        return R.layout.activity_wan_sheng_bao_gong;
    }

    @Override
    protected void initView() {
        button_submit = findViewById(R.id.button_submit);
        caigoudanhao = findViewById(R.id.caigoudanhao);
        jitaihao = findViewById(R.id.jitaihao);
        bencibaogong = findViewById(R.id.bencibaogong);
        gxName = findViewById(R.id.gxName);
        kebaoshuliang = findViewById(R.id.kebaoshuliang);
        button_clear = findViewById(R.id.button_clear);
        view_userName = findViewById(R.id.userName);
        caigoudanhao.setFocusable(true);
        caigoudanhao.requestFocus();
        timer = findViewById(R.id.timer);

        InputTextHelper.with(this)
                .addView(caigoudanhao)
                .setMain(jitaihao)
                .build();

        InputTextHelper.with(this)
                .addView(caigoudanhao)
                .addView(jitaihao)
                .addView(bencibaogong)
                .addView(kebaoshuliang)
                .addView(gxName)
                .setMain(button_submit)
                .build();
//        caigoudanhao.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                KeyboardUtils.hideKeyboard(v);
//                v.requestFocus();
//                return true;
//            }
//        });
//
//        jitaihao.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                KeyboardUtils.hideKeyboard(v);
//                v.requestFocus();
//                return true;
//            }
//        });
//        bencibaogong.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                KeyboardUtils.hideKeyboard(v);
//                v.requestFocus();
//                return true;
//            }
//        });

        //扫描机台后发送查询请求
        jitaihao.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (null != event && KeyEvent.KEYCODE_ENTER == event.getKeyCode()) {
                    switch (event.getAction()) {
                        case KeyEvent.ACTION_UP:
                            //做爱做的事情
                            if (caigoudanhao.getText().toString().indexOf("-", 0) > 0) {
                                int p1 = caigoudanhao.getText().toString().indexOf("-", 0);
                                int p2 = caigoudanhao.getText().toString().indexOf("-", p1 + 1);
                                String 生产加工单号 = caigoudanhao.getText().toString().substring(0, p1);
                                String 生产加工单id = caigoudanhao.getText().toString().substring(p1 + 1);
                                String 机台 = jitaihao.getText().toString();
                                EasyHttp.post(WanShengBaoGongActivity.this)
                                        .api(new WanShengBaoGongAPI().set生产加工单号(生产加工单号)
                                                .set生产加工单id(生产加工单id)
                                                .set机台(机台)
                                        )
                                        .request(new HttpCallback<HttpData<BaoGong>>(WanShengBaoGongActivity.this) {
                                            @Override
                                            public void onSucceed(HttpData<BaoGong> result) {
                                                baoGong = result.getData();
                                                baoGong.set生产加工单号(生产加工单号);
                                                baoGong.set生产加工单ID(生产加工单id);
                                                if (result.getData().get工序名称() != null) {
                                                    gxName.setText(result.getData().get工序名称());
                                                } else {
                                                    toast("没有该机台工序信息");
                                                }
                                                if (result.getData().get可报数量() != null) {
                                                    kebaoshuliang.setText(result.getData().get可报数量().toString());
                                                    bencibaogong.requestFocus();
                                                } else {
                                                    kebaoshuliang.setText("");
                                                    toast("未查询到该单信息");
                                                }
                                            }

                                            @Override
                                            public void onFail(Exception e) {
                                                jitaihao.setText("");
                                                gxName.setText("");
                                                super.onFail(e);
                                            }
                                        });
                                return true;
                            } else {
                                toast("订单条码缺少行号数据");
                                return true;
                            }
                        default:
                            return true;
                    }
                }
                return false;
            }
        });

        bencibaogong.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (null != event && KeyEvent.KEYCODE_ENTER == event.getKeyCode()) {
                    button_submit.requestFocus();
                }
                return false;
            }
        });

        setOnClickListener(button_submit, button_clear);
    }

    @Override
    protected void initData() {
        //获得用户名
        Intent intent = getIntent();
        userName = intent.getStringExtra("userName");
        userCode = intent.getStringExtra("userCode");
        view_userName.setText("用户名:" + userName);


    }


    @CheckNet
    @SingleClick
    @Override
    public void onClick(View v) {
        if (v == button_clear) {
            // 消息对话框
            new MessageDialog.Builder(this)
                    // 标题可以不用填写
                    .setTitle("警告")
                    // 内容必须要填写
                    .setMessage("确认清空所有内容?")
                    // 确定按钮文本
                    .setConfirm(getString(R.string.common_confirm))
                    // 设置 null 表示不显示取消按钮
                    .setCancel(getString(R.string.common_cancel))
                    // 设置点击按钮后不关闭对话框
                    //.setAutoDismiss(false)
                    .setListener(new MessageDialog.OnListener() {

                        @Override
                        public void onConfirm(BaseDialog dialog) {
                            caigoudanhao.setText("");
                            jitaihao.setText("");
                            gxName.setText("");
                            bencibaogong.setText("");
                            kebaoshuliang.setText("");
                            caigoudanhao.requestFocus();
                        }

                        @Override
                        public void onCancel(BaseDialog dialog) {
                            toast("取消了");
                        }
                    })
                    .show();
        }

        if (v == button_submit) {
            double benci = new Double(bencibaogong.getText().toString().trim());
            double zuida = new Double(kebaoshuliang.getText().toString().trim());
            if (gxName.getText().toString().equals("卷绕")) {
                new MessageDialog.Builder(this)
                        .setTitle("警告")
                        .setMessage("确定报工？")
                        .setConfirm(getString(R.string.common_confirm))
                        .setCancel(getString(R.string.common_cancel))
                        .setListener(new MessageDialog.OnListener() {
                            @Override
                            public void onConfirm(BaseDialog dialog) {
                                insert();
                            }

                            @Override
                            public void onCancel(BaseDialog dialog) {
                                toast("取消了");
                            }
                        }).show();
            } else {
                if (benci < zuida * 0.9) {
                    // 消息对话框
                    new MessageDialog.Builder(this)
                            // 标题可以不用填写
                            .setTitle("警告")
                            // 内容必须要填写
                            .setMessage("本次报工小于可报数量90%")
                            // 确定按钮文本
                            .setConfirm(getString(R.string.common_confirm))
                            // 设置 null 表示不显示取消按钮
                            .setCancel(getString(R.string.common_cancel))
                            // 设置点击按钮后不关闭对话框
                            //.setAutoDismiss(false)
                            .setListener(new MessageDialog.OnListener() {

                                @Override
                                public void onConfirm(BaseDialog dialog) {
                                    insert();
                                }

                                @Override
                                public void onCancel(BaseDialog dialog) {
                                    toast("取消了");
                                }
                            })
                            .show();
                } else if (benci >= zuida * 0.9 && benci <= zuida * 1.1) {
                    insert();
                } else if (benci > zuida * 1.1) {
                    // 消息对话框
                    new MessageDialog.Builder(this)
                            // 标题可以不用填写
                            .setTitle("警告")
                            // 内容必须要填写
                            .setMessage("本次报工大于可报数量110%，不可报工")
                            // 确定按钮文本
                            .setConfirm(getString(R.string.common_confirm))
                            // 设置 null 表示不显示取消按钮
                            .setCancel(getString(R.string.common_cancel))
                            // 设置点击按钮后不关闭对话框
                            //.setAutoDismiss(false)
                            .setListener(new MessageDialog.OnListener() {

                                @Override
                                public void onConfirm(BaseDialog dialog) {

                                }

                                @Override
                                public void onCancel(BaseDialog dialog) {
                                    toast("取消了");
                                }
                            })
                            .show();
                }
            }
        }
    }

    @SingleClick
    @Override
    public void onRightClick(View v) {
        if (true) {
            startActivity(LoginActivity.class);
            // 进行内存优化，销毁除登录页之外的所有界面
            ActivityStackManager.getInstance().finishAllActivities(LoginActivity.class);
            return;
        }
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeight) {

    }

    @Override
    public void onSoftKeyboardClosed() {

    }

    @Override
    public boolean isSwipeEnable() {
        return false;
    }

    public void insert() {
        EasyHttp.post(WanShengBaoGongActivity.this)
                .api(new WanShengBaoGongInsertAPI()
                        .set报工人员(userCode)
                        .set机台(jitaihao.getText().toString().trim())
                        .set报工数量(new BigDecimal(bencibaogong.getText().toString().trim()))
                        .set报工时间(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis())))
                        .setBaoGong(baoGong)
                )
                .request(new HttpCallback<HttpData<BGResponseBean>>(WanShengBaoGongActivity.this) {
                    @Override
                    public void onSucceed(HttpData<BGResponseBean> result) {
                        toast("报工成功");
                        caigoudanhao.setText("");
                        jitaihao.setText("");
                        gxName.setText("");
                        bencibaogong.setText("");
                        kebaoshuliang.setText("");
                        caigoudanhao.requestFocus();
                    }

                    @Override
                    public void onFail(Exception e) {
                        jitaihao.setText("");
                        gxName.setText("");
                        kebaoshuliang.setText("");
                        bencibaogong.setText("");
                        jitaihao.requestFocus();
                        super.onFail(e);
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        if (timers != null) {
//            timers.cancel();
//            timers = null;
//        }
//        if (task != null) {
//            task.cancel();
//            task = null;
//        }
    }
}

