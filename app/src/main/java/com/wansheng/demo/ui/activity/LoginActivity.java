package com.wansheng.demo.ui.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.wansheng.demo.R;
import com.wansheng.demo.aop.DebugLog;
import com.wansheng.demo.aop.SingleClick;
import com.wansheng.demo.common.MyActivity;
import com.wansheng.demo.helper.InputTextHelper;
import com.wansheng.demo.helper.KeyboardUtils;
import com.wansheng.demo.http.model.HttpData;
import com.wansheng.demo.http.request.LoginApi;
import com.wansheng.demo.http.response.LoginBean;
import com.wansheng.demo.http.response.User;
import com.wansheng.demo.other.AppConfig;
import com.wansheng.demo.other.IntentKey;
import com.wansheng.demo.other.KeyboardWatcher;
import com.wansheng.demo.wxapi.WXEntryActivity;
import com.hjq.http.EasyConfig;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.umeng.Platform;
import com.hjq.umeng.UmengClient;
import com.hjq.umeng.UmengLogin;

/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/AndroidProject
 * time   : 2018/10/18
 * desc   : 登录界面
 */
public final class LoginActivity extends MyActivity
        implements KeyboardWatcher.SoftKeyboardStateListener {


    private ImageView mLogoView;
    private ViewGroup mBodyLayout;
    private EditText mPhoneView;
    private View mBlankView;
    private TextView version;
    /**
     * logo 缩放比例
     */
    private final float mLogoScale = 0.8f;
    /**
     * 动画时间
     */
    private final int mAnimTime = 300;

    @Override
    protected int getLayoutId() {
        return R.layout.login_activity;
    }

    @Override
    protected void initView() {
        mLogoView = findViewById(R.id.iv_login_logo);
        mBodyLayout = findViewById(R.id.ll_login_body);
        mPhoneView = findViewById(R.id.et_login_phone);
        mBlankView = findViewById(R.id.v_login_blank);
        version = findViewById(R.id.version);
        mPhoneView.setFocusable(true);
        mPhoneView.requestFocus();

        mPhoneView.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (null != keyEvent && KeyEvent.KEYCODE_ENTER == keyEvent.getKeyCode()) {
                    //做爱做的事情
                    EasyHttp.get(LoginActivity.this)
                            .api(new LoginApi().setId(mPhoneView.getText().toString().trim()))
                            .request(new HttpCallback<HttpData<User>>(LoginActivity.this) {
                                @Override
                                public void onSucceed(HttpData<User> result) {
                                    if (result.getMessage().equals("登录成功")) {
                                        if (result.getData().get部门编号().equals("SCB06")) {
                                            Intent intent = new Intent(LoginActivity.this, WanShengBaoGongActivity.class);
                                            intent.putExtra("userName", result.getData().get员工姓名());
                                            intent.putExtra("userCode",mPhoneView.getText().toString());
                                            startActivity(intent);
                                            finish();
                                        }else if (result.getData().get部门编号().equals("PZB08")){
                                            Intent intent = new Intent(LoginActivity.this,WanShengQCActivity.class);
                                            intent.putExtra("userName", result.getData().get员工姓名());
                                            intent.putExtra("userCode",mPhoneView.getText().toString());
                                            startActivity(intent);
                                            finish();
                                        }
                                    }
                                    if (result.getMessage().equals("没有该用户信息")) {
                                        toast("没有该用户信息");
                                    }
                                }
                            });
                }
                return false;
            }
        });
//        mPhoneView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                KeyboardUtils.hideKeyboard(v);
//                v.requestFocus();
//                return true;
//            }
//        });
    }

    @Override
    protected void initData() {
        postDelayed(() -> {
            // 因为在小屏幕手机上面，因为计算规则的因素会导致动画效果特别夸张，所以不在小屏幕手机上面展示这个动画效果
            if (mBlankView.getHeight() > mBodyLayout.getHeight()) {
                // 只有空白区域的高度大于登录框区域的高度才展示动画
                KeyboardWatcher.with(LoginActivity.this)
                        .setListener(LoginActivity.this);
            }
        }, 500);
        version.setText("版本号："+AppConfig.getVersionName());
    }


    @SingleClick
    @Override
    public void onRightClick(View v) {
        startActivity(SettingActivity.class);
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeight) {
        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        int[] location = new int[2];
        // 获取这个 View 在屏幕中的坐标（左上角）
        mBodyLayout.getLocationOnScreen(location);
        //int x = location[0];
        int y = location[1];
        int bottom = screenHeight - (y + mBodyLayout.getHeight());
        if (keyboardHeight > bottom) {
            // 执行位移动画
            ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mBodyLayout, "translationY", 0, -(keyboardHeight - bottom));
            objectAnimator.setDuration(mAnimTime);
            objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            objectAnimator.start();

            // 执行缩小动画
            mLogoView.setPivotX(mLogoView.getWidth() / 2f);
            mLogoView.setPivotY(mLogoView.getHeight());
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator scaleX = ObjectAnimator.ofFloat(mLogoView, "scaleX", 1.0f, mLogoScale);
            ObjectAnimator scaleY = ObjectAnimator.ofFloat(mLogoView, "scaleY", 1.0f, mLogoScale);
            ObjectAnimator translationY = ObjectAnimator.ofFloat(mLogoView, "translationY", 0.0f, -(keyboardHeight - bottom));
            animatorSet.play(translationY).with(scaleX).with(scaleY);
            animatorSet.setDuration(mAnimTime);
            animatorSet.start();
        }
    }

    @Override
    public void onSoftKeyboardClosed() {
        // 执行位移动画
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(mBodyLayout, "translationY", mBodyLayout.getTranslationY(), 0);
        objectAnimator.setDuration(mAnimTime);
        objectAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        objectAnimator.start();

        if (mLogoView.getTranslationY() == 0) {
            return;
        }
        // 执行放大动画
        mLogoView.setPivotX(mLogoView.getWidth() / 2f);
        mLogoView.setPivotY(mLogoView.getHeight());
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mLogoView, "scaleX", mLogoScale, 1.0f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mLogoView, "scaleY", mLogoScale, 1.0f);
        ObjectAnimator translationY = ObjectAnimator.ofFloat(mLogoView, "translationY", mLogoView.getTranslationY(), 0);
        animatorSet.play(translationY).with(scaleX).with(scaleY);
        animatorSet.setDuration(mAnimTime);
        animatorSet.start();
    }

    @Override
    public boolean isSwipeEnable() {
        return false;
    }

}