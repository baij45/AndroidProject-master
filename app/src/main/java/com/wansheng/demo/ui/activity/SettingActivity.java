package com.wansheng.demo.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.Gravity;
import android.view.View;

import com.hjq.base.BaseDialog;
import com.wansheng.demo.R;
import com.wansheng.demo.aop.SingleClick;
import com.wansheng.demo.common.MyActivity;
import com.wansheng.demo.helper.ActivityStackManager;
import com.wansheng.demo.helper.CacheDataManager;
import com.wansheng.demo.http.glide.GlideApp;
import com.wansheng.demo.http.model.HttpData;
import com.wansheng.demo.http.request.LogoutApi;
import com.wansheng.demo.other.AppConfig;
import com.wansheng.demo.ui.dialog.InputDialog;
import com.wansheng.demo.ui.dialog.MenuDialog;
import com.wansheng.demo.ui.dialog.SafeDialog;
import com.wansheng.demo.ui.dialog.UpdateDialog;
import com.hjq.http.EasyHttp;
import com.hjq.http.listener.HttpCallback;
import com.hjq.widget.layout.SettingBar;
import com.hjq.widget.view.SwitchButton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * author : Android 轮子哥
 * github : https://github.com/getActivity/AndroidProject
 * time   : 2019/03/01
 * desc   : 设置界面
 */
public final class SettingActivity extends MyActivity
        implements SwitchButton.OnCheckedChangeListener {

    private SettingBar mPhoneView;
    private SettingBar mPasswordView;
    private SettingBar mCleanCacheView;

    @Override
    protected int getLayoutId() {
        return R.layout.setting_activity;
    }

    @Override
    protected void initView() {

        mPhoneView = findViewById(R.id.sb_setting_phone);
        mPasswordView = findViewById(R.id.sb_setting_password);
        mCleanCacheView = findViewById(R.id.sb_setting_cache);


        setOnClickListener(R.id.sb_setting_phone, R.id.sb_setting_password, R.id.sb_setting_cache);
    }

    @Override
    protected void initData() {
        // 获取应用缓存大小
        mCleanCacheView.setRightText(CacheDataManager.getTotalCacheSize(this));
        //获得IP
        SharedPreferences sharedPreferences = ActivityStackManager.getInstance().getApplication().getSharedPreferences("ip", Context.MODE_PRIVATE);
        String ip = sharedPreferences.getString("ip", "192.168.19.1");
        mPhoneView.setRightText(ip);
        //获得端口
        SharedPreferences sharedPreferences1 = ActivityStackManager.getInstance().getApplication().getSharedPreferences("port", Context.MODE_PRIVATE);
        String port = sharedPreferences1.getString("port", "8888");
        mPasswordView.setRightText(port);
    }

    @SingleClick
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sb_setting_phone:
                // 输入对话框
                new InputDialog.Builder(this)
                        // 标题可以不用填写
                        .setTitle("修改IP")
                        // 提示可以不用填写
                        .setHint("IP")
                        // 确定按钮文本
                        .setConfirm(getString(R.string.common_confirm))
                        // 设置 null 表示不显示取消按钮
                        .setCancel(getString(R.string.common_cancel))
                        // 设置点击按钮后不关闭对话框
                        //.setAutoDismiss(false)
                        .setListener(new InputDialog.OnListener() {

                            @Override
                            public void onConfirm(BaseDialog dialog, String content) {
                                Pattern pattern = Pattern.compile("(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)");
                                Matcher matcher = pattern.matcher(content);
                                if (matcher.matches()) {
                                    //IP符合格式，保存到SharedPreferences
                                    SharedPreferences sharedPreferences = getSharedPreferences("ip", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("ip", content).commit();
                                    mPhoneView.setRightText(content);
                                } else {
                                    toast("输入的IP地址不符合格式");
                                }
                            }

                            @Override
                            public void onCancel(BaseDialog dialog) {
                                toast("取消了");
                            }
                        })
                        .show();
                break;
            case R.id.sb_setting_password:
                // 输入对话框
                new InputDialog.Builder(this)
                        // 标题可以不用填写
                        .setTitle("修改端口")
                        // 提示可以不用填写
                        .setHint("端口号")
                        // 确定按钮文本
                        .setConfirm(getString(R.string.common_confirm))
                        // 设置 null 表示不显示取消按钮
                        .setCancel(getString(R.string.common_cancel))
                        // 设置点击按钮后不关闭对话框
                        //.setAutoDismiss(false)
                        .setListener(new InputDialog.OnListener() {

                            @Override
                            public void onConfirm(BaseDialog dialog, String content) {
                                Pattern pattern = Pattern.compile("^([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])(,([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5]))*$|^([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])-([0-9]|[1-9]\\d|[1-9]\\d{2}|[1-9]\\d{3}|[1-5]\\d{4}|6[0-4]\\d{3}|65[0-4]\\d{2}|655[0-2]\\d|6553[0-5])$");
                                Matcher matcher = pattern.matcher(content);
                                if (matcher.matches()) {
                                    //端口符合格式，保存到SharedPreferences
                                    //  EasyConfig.getInstance().setServer()
                                    SharedPreferences sharedPreferences = getSharedPreferences("port", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putString("port", content).commit();
                                    mPasswordView.setRightText(content);
                                } else {
                                    toast("输入的端口号不符合格式");
                                }
                            }

                            @Override
                            public void onCancel(BaseDialog dialog) {
                                toast("取消了");
                            }
                        })
                        .show();
                break;
            case R.id.sb_setting_cache:
                // 清除内存缓存（必须在主线程）
                GlideApp.get(getActivity()).clearMemory();
                new Thread(() -> {
                    CacheDataManager.clearAllCache(this);
                    // 清除本地缓存（必须在子线程）
                    GlideApp.get(getActivity()).clearDiskCache();
                    post(() -> {
                        // 重新获取应用缓存大小
                        mCleanCacheView.setRightText(CacheDataManager.getTotalCacheSize(getActivity()));
                    });
                }).start();
                break;
            default:
                break;
        }
    }

    /**
     * {@link SwitchButton.OnCheckedChangeListener}
     */

    @Override
    public void onCheckedChanged(SwitchButton button, boolean isChecked) {
        toast(isChecked);
    }
}